NO 1. BUAT DATABASE.............................................
CREATE DATABASE myshop;

NO 2. MEMBUAT TABLE.............................................
-Table users
CREATE TABLE users( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

-Table categories
CREATE TABLE catagories( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null);

-Table items
CREATE TABLE items( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) NOT null, description varchar(255) NOT null, price int NOT null, stock int NOT null, category_id int NOT null, FOREIGN KEY(category_id) REFERENCES categories );

NO 3. MASUKKAN DATA..............................................
-table users
INSERT INTO users(name, email, password) VALUES("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe", "jenita123");

-table categories
INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");

-tablle items
INSERT INTO items(name, description, price, stock, category_id) VALUES("Sumsang b50", "hape keren dan merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

NO 4..............................................................
A.tanpa password
-SELECT name, email;

B.
a.diatas 10000000
-SELECT * from items where price > 1000000;

b.name serupa
-SELECT * from items where name LIKE '%watch';

c.gabung table
-SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name from items INNER join categories ON items.category_id = categories.id;

NO 5 MENGUBAH DATA
-UPDATE items set price=2500000 where id = 1;  

